(function ($, window, document, undefined) {
  "use strict";

  var $years = $(".Company-history-year");
  var yearSelector = ".Company-history-year";
  var content = $(".Company-history-content");
  var $dot = $(".Company-history-dot");
  var $hoverLine = $(".dot-hover-line");
  var hoverLineClass = ".dot-hover-line";
  var hoverLineStr = "dot-hover-line";
  var dotActiveClass = "Company-history-dot-active";
  var dotHoverClass = "Company-history-dot-hover";
  var sliderDotYear = ".Company-history-dot-year"; // Year label in the dots navigation
  var sliderDotYearClass = "Company-history-dot-year";
  var activeYearClass = "Company-history-year-active";
  var $activeYear = $(".Company-history-year-active");
  var contentSlide1 = "#js-history-content-slide1";
  var contentSlide2 = "#js-history-content-slide2";
  var contentSlide3 = "#js-history-content-slide3";
  var contentSlide4 = "#js-history-content-slide4";
  var contentSlide5 = "#js-history-content-slide5";
  var contentSlide6 = "#js-history-content-slide6";
  var contentSlide7 = "#js-history-content-slide7";
  var contentSlide8 = "#js-history-content-slide8";
  var yearPos = ["y-p-1", "y-p-2", "y-p-3", "y-p-4", "y-p-5", "y-p-6", "y-p-7"];
  var yearPosRight = ["p-1", "p-2", "p-3", "p-4", "p-5", "p-6", "p-7", "p-8"];
  var contPos = ["c-p-1", "c-p-2", "c-p-3", "c-p-4", "c-p-5", "c-p-6", "c-p-7", "c-p-8"];
  var contPosReversed = contPos.reverse();

  function deactivateDot(dot) {
    dot.siblings().removeClass(dotActiveClass + " " + dotHoverClass).find(hoverLineClass).remove();
    dot.siblings().find(sliderDotYear).remove();
  }

  function repositionYears(year) {
    if ($(year).attr("data-type") === "right") {
      var _allRight = $(year).prevAll(yearSelector);
      /* eslint-disable block-scoped-var, no-redeclare */
      for (var ii = 0; ii <= _allRight.length; ii++) {
        $(_allRight[ii]).attr("data-type", "left").removeClass(yearPosRight.join(" "));
        $(year).removeClass(yearPosRight.join(" "));
        $($(yearSelector)[ii]).addClass(yearPos[ii - 1]);
      }
      $(year).attr("data-type", "left");

      content.removeClass(contPos[ii - 2]);
      content.addClass(contPos[ii - 1]);
    }

    if ($(year).attr("data-type") === "left") {
      var _allLeft = $(year).nextAll(yearSelector);
      var _index = $(year).index();

      for (var jj = 0; jj <= _allLeft.length; jj++) {
        $(_allLeft[jj]).attr("data-type", "right").addClass(yearPosRight[_index++]);

        content.removeClass(contPos.join(" "));
        content.addClass(contPosReversed[jj]);
      }
    }
  }

  function fillContent(index) {
    if (index === 0) {
      content.html($(contentSlide1).html());
    }
    if (index === 1) {
      content.html($(contentSlide2).html());
    }
    if (index === 2) {
      content.html($(contentSlide3).html());
    }
    if (index === 3) {
      content.html($(contentSlide4).html());
    }
    if (index === 4) {
      content.html($(contentSlide5).html());
    }
    if (index === 5) {
      content.html($(contentSlide6).html());
    }
    if (index === 6) {
      content.html($(contentSlide7).html());
    }
    if (index === 7) {
      content.html($(contentSlide8).html());
    }
  }

  $years.on("click", function () {
    var year = $(this);
    year.siblings().removeClass(activeYearClass);
    year.addClass(activeYearClass);
    var yearIndex = $(yearSelector).index(this);

    repositionYears(year);

    fillContent(yearIndex);

    deactivateDot($dot);

    if (!$($years[yearIndex]).is(":last-child")) {
      $($dot[yearIndex]).append("<span class=" + hoverLineStr + "></span>").addClass(dotActiveClass).append("<span class=" + sliderDotYearClass + ">" + $(this).text() + "</span>");
      $hoverLine.css({"background-color": "#11a2d1"});
    } else {
      $($dot[yearIndex]).addClass(dotActiveClass).append("<span class=" + sliderDotYearClass + ">" + $(this).text() + "</span>");
      $hoverLine.css({"background-color": "#11a2d1"});
    }
    if (!$dot.hasClass(dotActiveClass)) {
      $dot.append("<span class=" + sliderDotYearClass + ">" + $($years[$dot.index()]).text() + "</span>");
    }
  });

  $years.on({
    "mouseenter": function () {
      var yearIndex = $years.index(this);

      if (!$($years[yearIndex]).is(":last-child")) {
        $($dot[yearIndex]).append("<span class=" + hoverLineStr + "></span>").addClass(dotHoverClass).append("<span class=" + sliderDotYearClass + ">" + $(this).text() + "</span>");
        $hoverLine.css({"background-color": "#11a2d1"});
      } else {
        $($dot[yearIndex]).addClass(dotHoverClass).append("<span class=" + sliderDotYearClass + ">" + $(this).text() + "</span>");
        $hoverLine.css({"background-color": "#11a2d1"});
      }

      if (!$dot.hasClass(dotActiveClass)) {
        $dot.append("<span class=" + sliderDotYearClass + ">" + $($years[$dot.index()]).text() + "</span>");
      }
    },
    "mouseleave": function () {
      var yearIndex = $years.index(this);

      if (!$($dot[yearIndex]).hasClass(dotActiveClass)) {
        $($dot[yearIndex]).removeClass(dotHoverClass).find(hoverLineClass).remove();
        $($dot[yearIndex]).find(sliderDotYear).remove();
      }
    }
  });

  $dot.on({
    "mouseenter": function () {
      var yearHoverHeight = "190px";
      var dot = $(this);

      if (!dot.is(":last-child")) {
        dot.append("<span class=" + hoverLineStr + "></span>");
        $hoverLine.css({"background-color": "#11a2d1"});
      }
      if (!dot.hasClass(dotActiveClass)) {
        $($($years[dot.index()])).css({"height": yearHoverHeight});
        dot.append("<span class=" + sliderDotYearClass + ">" + $($years[dot.index()]).text() + "</span>");
      }

      $hoverLine.css({"background-color": "#11a2d1"});
    },
    "mouseleave": function () {
      var dot = $(this);

      if (!dot.hasClass(dotActiveClass)) {
        $hoverLine.css({"background-color": "#5b6aaa"});

        $($($years[dot.index()])).removeAttr("style");

        dot.find(sliderDotYear).remove();
        dot.find(hoverLineClass).remove();
      }
    }
  });

  $dot.on("click", function () {
    var dot = $(this);
    var year = $($years[dot.index()]);
    var yearIndex = $(year).index() - 1;

    deactivateDot(dot);
    dot.addClass(dotActiveClass);
    year.removeAttr("style").addClass(activeYearClass);
    fillContent(dot.index());
    year.siblings().removeClass(activeYearClass);
    $(yearIndex).addClass(activeYearClass);
    repositionYears(year);
  });

  $(document).on("ready", function () {
    $dot.first().append("<span class=" + sliderDotYearClass + ">" + $activeYear.text() + "</span>")
            .addClass(dotActiveClass)
            .append("<span class=" + hoverLineStr + "></span>");
    content.html($(contentSlide1).html());
  });
})(jQuery, window, document);
